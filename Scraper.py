from bs4 import BeautifulSoup
import requests as req
import csv
from collections import Counter
import re

def find_top3(wc):#find 3 most frequently used words in counter dict
	first=("test",0)#most used word
	second=("test2",0)#second most used
	third=("test3",0)#third most used
	for word in wc:#loop through all word:count pairs in dict
		if wc[word]>first[1]:#if count is higher than current first
			third=second#move first, second one down
			second=first
			first=(word,wc[word])#reassign first
		elif wc[word]>second[1]:#do the same for second and third word
			third=second
			second=(word, wc[word])
		elif wc[word]>third[1]:
			third=(word, wc[word])
	return (first, second, third)#return top 3 as tuple


def doStuff():
	fo=open('heise_http.csv','w')#open file in write mode (if existing) and emptiy it; otherwise create file
	csvw = csv.writer(fo, delimiter = ';')#instantiate csv writer; use ';' as delimiter
	for page in range(4):			#search for "HTTPS" returns 4 pages
		qstr="https://www.heise.de/thema/https?seite="+str(page)#iterate through every one of these pages
		con=BeautifulSoup(req.get(qstr).text, "lxml").find("aside")#get the page content as lxml and find "aside" element
		con=con.find("nav")#find "nav" element within aside
		c=con.findAll("header")#find all headers (article titles) within nav
		
		full_count=Counter()
		for x in c:#loop through all header elements found
			txt=[""]#initialize empty 
			#split string into words, remove punctuation and newlines, encode, set to lowercase and count words
			word_count=Counter(re.findall(ur"[\w']+", x.text.rstrip().encode('utf-8').lower(), re.UNICODE))
			full_count=word_count+full_count#join with count of earlier lines
			txt.append(x.text.rstrip().encode('utf-8'))#add string without newlines
			csvw.writerow(txt)#write to csv file
	top3=find_top3(full_count)#find top 3 words in counter dictionary
	print("3 most used words:")
	print("1. "+top3[0][0].decode('utf-8')+' ('+str(top3[0][1])+" times)")
	print("2. "+top3[1][0].decode('utf-8')+' ('+str(top3[1][1])+" times)")
	print("3. "+top3[2][0].decode('utf-8')+' ('+str(top3[2][1])+" times)")
	fo.close()#close file

if __name__=="__main__":
	doStuff()

